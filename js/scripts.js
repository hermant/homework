$('document').ready(function() {
    var $imgs = $('#slideshow > img'), current = 0;

    var nextImage = function() {
        if (current >= $imgs.length) current = 0;
        $imgs.eq(current++).fadeIn(function() {
            $(this).delay(3000).fadeOut(nextImage);
        })
    };
    nextImage();
});