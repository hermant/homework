<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Herman">

    <!--bootstrap-->
    <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <title>Homework</title>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Homework</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <section class="section-slider" id="slideshow">
        <img src="/images/peacock.jpg" alt="">
        <img src="/images/volkswagen.jpg" alt="">
    </section>

    <section class="main-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Homework</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus cursus nisi sit amet pulvinar pharetra. Suspendisse aliquet placerat felis quis porttitor. Sed quis dolor mattis, laoreet nunc ut, scelerisque dui.</p>
                    <p>Sed condimentum metus eu ante malesuada, eget fermentum ipsum sagittis. Aenean a nunc ex. Nulla nunc mi, euismod ut lorem sit amet, tempor porta magna. In consequat nisl sed eros molestie, ut vehicula odio gravida. Suspendisse feugiat sapien tortor, a consectetur ante fermentum vel. Integer mattis venenatis ante, non vulputate orci feugiat ut. Donec facilisis, arcu facilisis viverra consectetur, lorem magna lacinia nisi, vel posuere ex quam ac orci. Morbi porta pulvinar sem, maximus sodales libero. In libero ante, ultrices vel tortor in, euismod aliquam est. Proin vulputate quis est nec lacinia. Suspendisse lacus arcu, pharetra euismod nibh non, rhoncus vehicula magna. Cras pellentesque nisi at ipsum aliquam, consequat aliquam felis accumsan. Curabitur eu malesuada velit, et volutpat risus.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="flexbox-section">
        <div class="container-flex">
            <article>
                <a href="">
                    <h2>Heading 2</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </a>
            </article>
            <article>
                <a href="">
                    <h2>Heading 2</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </a>
            </article>
            <article>
                <a href="">
                    <h2>Heading 2</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </a>
            </article>

        </div>
    </section>

    <section class="flexbox-section">
        <div class="container-flex">
            <article class="text-center">
                <a href="">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 286.082 286.082" style="enable-background:new 0 0 286.082 286.082;" xml:space="preserve">
                        <g>
                            <path d="M143.097,219.074c-9.905,0-17.95,8.01-17.95,17.86c0,9.869,8.036,17.86,17.95,17.86
                                s17.941-8.001,17.941-17.86S153.011,219.074,143.097,219.074z M281.199,84.906c-36.91-34.577-85.977-53.617-138.155-53.617
                                S41.798,50.329,4.888,84.924c-6.517,6.105-6.517,15.992,0,22.107c3.254,3.048,7.527,4.577,11.791,4.577s8.537-1.529,11.8-4.577
                                C59.078,78.345,99.769,64.23,143.035,64.23c43.284,0,83.966,14.115,114.565,42.783c6.517,6.105,17.074,6.105,23.599,0
                                C287.706,100.907,287.715,91.011,281.199,84.906z M142.901,93.809c-38.564,0-74.821,14.911-102.094,41.978
                                c-6.803,6.758-6.803,17.709,0,24.467c3.406,3.388,7.866,5.077,12.327,5.077s8.921-1.69,12.318-5.077
                                c20.685-20.533,48.2-32.968,77.449-32.968c29.24,0,56.764,12.443,77.44,32.968c6.812,6.758,17.843,6.758,24.645,0
                                c6.812-6.758,6.812-17.709,0-24.467C217.713,108.72,181.446,93.809,142.901,93.809z M143.053,156.357
                                c-21.812,0-42.309,8.751-57.738,24.636c-6.392,6.579-6.392,17.271,0,23.85c3.182,3.281,7.375,4.925,11.567,4.925
                                c4.184,0,8.367-1.645,11.558-4.925c9.243-9.529,21.535-15.331,34.613-15.331c13.06,0,25.352,5.793,34.613,15.313
                                c6.392,6.579,16.734,6.579,23.135,0c6.374-6.579,6.374-17.253,0-23.832C185.371,165.108,164.864,156.357,143.053,156.357z"/>
                        </g>
                    </svg>
                </a>
            </article>
            <article class="text-center">
                <a href="">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 268.175 268.175" style="enable-background:new 0 0 268.175 268.175;" xml:space="preserve">
                    <g>
                        <path d="M35.757,143.027H17.878C8.01,143.027,0,151.036,0,160.905v53.635c0,9.869,8.01,17.878,17.878,17.878
                            h17.878c9.869,0,17.878-8.01,17.878-17.878v-53.635C53.635,151.036,45.626,143.027,35.757,143.027z M107.27,107.27H89.392
                            c-9.869,0-17.878,8.01-17.878,17.878v89.392c0,9.869,8.01,17.878,17.878,17.878h17.878c9.869,0,17.878-8.01,17.878-17.878v-89.392
                            C125.149,115.28,117.139,107.27,107.27,107.27z M178.784,71.513h-17.878c-9.869,0-17.878,8.01-17.878,17.878V214.54
                            c0,9.869,8.01,17.878,17.878,17.878h17.878c9.869,0,17.878-8.01,17.878-17.878V89.392
                            C196.662,79.523,188.653,71.513,178.784,71.513z M250.297,35.757h-17.878c-9.869,0-17.878,8.01-17.878,17.878V214.54
                            c0,9.869,8.01,17.878,17.878,17.878h17.878c9.869,0,17.878-8.01,17.878-17.878V53.635
                            C268.176,43.766,260.166,35.757,250.297,35.757z"/>
                    </g>
                    </svg>

                </a>
            </article>
            <article class="text-center">
                <a href="">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 286.107 286.107" style="enable-background:new 0 0 286.107 286.107;" xml:space="preserve">
                    <g>
                        <path d="M237.891,54.663c-10.638-10.772-20.149-19.353-33.978-26.129c-8.689-4.264-19.175-0.59-23.376,8.197
                            c-4.228,8.787-0.581,19.362,8.099,23.626c10.298,5.042,17.217,10.62,25.164,18.665c39.011,39.449,39.011,103.632,0,143.072
                            c-18.897,19.121-44.043,29.624-70.763,29.624c-26.728,0-51.865-10.512-70.763-29.624c-39.011-39.44-39.011-103.623,0-143.072
                            c7.974-8.054,15.858-13.614,25.647-18.647c8.269-4.273,11.728-14.839,7.715-23.635c-4.014-8.778-12.649-12.39-21.123-8.242
                            c-13.436,6.597-25.647,15.393-36.311,26.165c-52.303,52.875-52.303,138.897,0,191.781c26.156,26.433,60.5,39.663,94.845,39.663
                            c34.353,0,68.698-13.221,94.845-39.663C290.212,193.578,290.212,107.547,237.891,54.663z M143.064,143.027
                            c9.869,0,17.878-8.01,17.878-17.878V17.878C160.942,8.01,152.933,0,143.064,0s-17.878,8.01-17.878,17.878v107.27
                            C125.186,135.026,133.195,143.027,143.064,143.027z"/>
                    </g>
                    </svg>
                </a>
            </article>
        </div>
    </section>

    <footer>
        <nav class="navbar">
            <div class="container">
                <div class="col-xs-12">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </footer>

    <script src="components/jquery/dist/jquery.min.js"></script>
    <script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>
</html>
